<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\View;

class UtilityController extends Controller
{
    public function recordTransfer()
    {
        $path = storage_path() . "/app/public/records.json";
        $json = json_decode(file_get_contents($path), true);

        try
        {
            foreach (array_chunk($json["RECORDS"],1000) as $data)
            {
                DB::table('records')->insert($data);
            }
            $message = "Data has been imported";
            return View::make('message', compact('message'));
        }
        catch (Exception $exception)
        {
            $message = "Data cannot be imported";
            return View::make('message', compact('message'));
        }
    }

    public function defineCallbackJS()
    {
        $message = "See console for sample output. The 'checkAge()' function has been used exactly like the provided sample in the problem-set. The script can be found at message.blade.php";
        return View::make('message', compact('message'));
    }

    public function sortJS()
    {
        $message = "Please check console for the output";
        return View::make('sort', compact('message'));
    }

    public function filterJS()
    {
        $message = "Please check console for the output";
        return View::make('filter', compact('message'));
    }

    public function mapJS()
    {
        $message = "Please check console for the output";
        return View::make('map', compact('message'));
    }

    public function reduceJS()
    {
        $message = "Please check console for the output";
        return View::make('reduce', compact('message'));
    }

    public function animation()
    {
        $message = "";
        return View::make('animation', compact('message'));
    }

    public function funny()
    {
        $message = "";
        return View::make('funny', compact('message'));
    }
}
