<?php

namespace App\Http\Controllers;

use App\Buyer;
use App\DiaryTaken;
use App\EraserTaken;
use App\PenTaken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use StdClass;

class BuyerController extends Controller
{
    public function secondBuyerEloq()
    {
        $buyers = Buyer::all();
        $buyerList = Array();
        foreach ($buyers as $buyer) {
            $buyerList[$buyer->id] = new StdClass();
            $buyerList[$buyer->id]->buyerID = $buyer->id;
            $buyerList[$buyer->id]->total = 0;
            $buyerList[$buyer->id]->totalDiary = 0;
            $buyerList[$buyer->id]->totalEraser = 0;
            $buyerList[$buyer->id]->totalPen = 0;
            $buyerList[$buyer->id]->name = $buyer->name;
        }

        $diaryTaken = DiaryTaken::select('buyer_id', DB::raw('SUM(amount) as total'))
            ->groupBy('buyer_id')
            ->get();
        foreach ($diaryTaken as $diary)
        {
            $buyerList[$diary->buyer_id]->totalDiary = $diary->total;
            $buyerList[$diary->buyer_id]->total += $diary->total;
        }

        $eraserTaken = EraserTaken::select('buyer_id', DB::raw('SUM(amount) as total'))
            ->groupBy('buyer_id')
            ->get();
        foreach ($eraserTaken as $eraser)
        {
            $buyerList[$eraser->buyer_id]->totalEraser = $eraser->total;
            $buyerList[$eraser->buyer_id]->total += $eraser->total;
        }

        $penTaken = PenTaken::select('buyer_id', DB::raw('SUM(amount) as total'))
            ->groupBy('buyer_id')
            ->get();
        foreach ($penTaken as $pen)
        {
            $buyerList[$pen->buyer_id]->totalPen = $pen->total;
            $buyerList[$pen->buyer_id]->total += $pen->total;
        }

        usort($buyerList, function($firstElement, $secondElement) {
            return $firstElement->total > $secondElement->total? -1: 1;
        });

        $buyerDetail = array_slice($buyerList, 1, 1, true);
//        var_dump($buyerDetail);
//        $buyerDetail = $buyerList;
//        var_dump(json_encode($buyerDetail));
        return View::make('secondBuyer', compact('buyerDetail'));
    }
    public function secondBuyerNoEloq()
    {
        $buyers = DB::select('select * from buyers');
        $buyerList = Array();
        foreach ($buyers as $buyer) {
            $buyerList[$buyer->id] = new StdClass();
            $buyerList[$buyer->id]->buyerID = $buyer->id;
            $buyerList[$buyer->id]->total = 0;
            $buyerList[$buyer->id]->totalDiary = 0;
            $buyerList[$buyer->id]->totalEraser = 0;
            $buyerList[$buyer->id]->totalPen = 0;
            $buyerList[$buyer->id]->name = $buyer->name;
        }

        $diaryTaken = DB::select('select buyer_id, SUM(amount) as total from diary_taken group by buyer_id');
        foreach ($diaryTaken as $diary)
        {
            $buyerList[$diary->buyer_id]->totalDiary = $diary->total;
            $buyerList[$diary->buyer_id]->total += $diary->total;
        }

        $eraserTaken = DB::select('select buyer_id, SUM(amount) as total from eraser_taken group by buyer_id');
        foreach ($eraserTaken as $eraser)
        {
            $buyerList[$eraser->buyer_id]->totalEraser = $eraser->total;
            $buyerList[$eraser->buyer_id]->total += $eraser->total;
        }

        $penTaken = DB::select('select buyer_id, SUM(amount) as total from pen_taken group by buyer_id');
        foreach ($penTaken as $pen)
        {
            $buyerList[$pen->buyer_id]->totalPen = $pen->total;
            $buyerList[$pen->buyer_id]->total += $pen->total;
        }

        usort($buyerList, function($firstElement, $secondElement) {
            return $firstElement->total > $secondElement->total? -1: 1;
        });

        $buyerDetail = array_slice($buyerList, 1, 1, true);
//        var_dump($buyerDetail);
//        $buyerDetail = $buyerList;
//        var_dump(json_encode($diaryTaken));
        return View::make('secondBuyer', compact('buyerDetail'));
    }

    public function purchaseListEloq()
    {
        $buyers = Buyer::all();
        $buyerList = Array();
        foreach ($buyers as $buyer) {
            $buyerList[$buyer->id] = new StdClass();
            $buyerList[$buyer->id]->buyerID = $buyer->id;
            $buyerList[$buyer->id]->total = 0;
            $buyerList[$buyer->id]->totalDiary = 0;
            $buyerList[$buyer->id]->totalEraser = 0;
            $buyerList[$buyer->id]->totalPen = 0;
            $buyerList[$buyer->id]->name = $buyer->name;
        }

        $diaryTaken = DiaryTaken::select('buyer_id', DB::raw('SUM(amount) as total'))
            ->groupBy('buyer_id')
            ->get();
        foreach ($diaryTaken as $diary)
        {
            $buyerList[$diary->buyer_id]->totalDiary = $diary->total;
            $buyerList[$diary->buyer_id]->total += $diary->total;
        }

        $eraserTaken = EraserTaken::select('buyer_id', DB::raw('SUM(amount) as total'))
            ->groupBy('buyer_id')
            ->get();
        foreach ($eraserTaken as $eraser)
        {
            $buyerList[$eraser->buyer_id]->totalEraser = $eraser->total;
            $buyerList[$eraser->buyer_id]->total += $eraser->total;
        }

        $penTaken = PenTaken::select('buyer_id', DB::raw('SUM(amount) as total'))
            ->groupBy('buyer_id')
            ->get();
        foreach ($penTaken as $pen)
        {
            $buyerList[$pen->buyer_id]->totalPen = $pen->total;
            $buyerList[$pen->buyer_id]->total += $pen->total;
        }

        usort($buyerList, function($firstElement, $secondElement) {
            return $firstElement->total < $secondElement->total? -1: 1;
        });

//        $buyerDetail = array_slice($buyerList, 1, 1, true);
//        var_dump($buyerDetail);
        $buyerDetail = $buyerList;
//        var_dump(json_encode($buyerDetail));
        return View::make('secondBuyer', compact('buyerDetail'));
    }

    public function purchaseListNoEloq()
    {
        $buyers = DB::select('select * from buyers');
        $buyerList = Array();
        foreach ($buyers as $buyer) {
            $buyerList[$buyer->id] = new StdClass();
            $buyerList[$buyer->id]->buyerID = $buyer->id;
            $buyerList[$buyer->id]->total = 0;
            $buyerList[$buyer->id]->totalDiary = 0;
            $buyerList[$buyer->id]->totalEraser = 0;
            $buyerList[$buyer->id]->totalPen = 0;
            $buyerList[$buyer->id]->name = $buyer->name;
        }

        $diaryTaken = DB::select('select buyer_id, SUM(amount) as total from diary_taken group by buyer_id');
        foreach ($diaryTaken as $diary)
        {
            $buyerList[$diary->buyer_id]->totalDiary = $diary->total;
            $buyerList[$diary->buyer_id]->total += $diary->total;
        }

        $eraserTaken = DB::select('select buyer_id, SUM(amount) as total from eraser_taken group by buyer_id');
        foreach ($eraserTaken as $eraser)
        {
            $buyerList[$eraser->buyer_id]->totalEraser = $eraser->total;
            $buyerList[$eraser->buyer_id]->total += $eraser->total;
        }

        $penTaken = DB::select('select buyer_id, SUM(amount) as total from pen_taken group by buyer_id');
        foreach ($penTaken as $pen)
        {
            $buyerList[$pen->buyer_id]->totalPen = $pen->total;
            $buyerList[$pen->buyer_id]->total += $pen->total;
        }

        usort($buyerList, function($firstElement, $secondElement) {
            return $firstElement->total < $secondElement->total? -1: 1;
        });

//        $buyerDetail = array_slice($buyerList, 1, 1, true);
//        var_dump($buyerDetail);
        $buyerDetail = $buyerList;
//        var_dump(json_encode($diaryTaken));
        return View::make('secondBuyer', compact('buyerDetail'));
    }

}
