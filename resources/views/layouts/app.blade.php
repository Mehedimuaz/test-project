<!DOCTYPE html>
<html>
<head>
    <style>
        .navbar-link
        {
            margin-left: 5vw;
            margin-bottom: 5vh;
        }
    </style>
</head>

<body>
<span class="navbar-link"><a href="{{url('second-buyer-eloquent')}}">second-buyer-eloquent</a></span>
<span class="navbar-link"><a href="{{url('second-buyer-no-eloquent')}}">second-buyer-no-eloquent</a></span>
<span class="navbar-link"><a href="{{url('purchase-list-eloquent')}}">purchase-list-eloquent</a></span>
<span class="navbar-link"><a href="{{url('purchase-list-no-eloquent')}}">purchase-list-no-eloquent</a></span>
<span class="navbar-link"><a href="{{url('record-transfer')}}">record-transfer</a></span>
<span class="navbar-link"><a href="{{url('define-callback-js')}}">define-callback-js</a></span>
<span class="navbar-link"><a href="{{url('sort-js')}}">sort-js</a></span>
<span class="navbar-link"><a href="{{url('filter-js')}}">filter-js</a></span>
<span class="navbar-link"><a href="{{url('map-js')}}">map-js</a></span>
<span class="navbar-link"><a href="{{url('reduce-js')}}">reduce-js</a></span>
<span class="navbar-link"><a href="{{url('animation')}}">animation</a></span>
<span class="navbar-link"><a href="{{url('i-m-funny')}}">i-m-funny</a></span>

<div style="top: 10vh; position: absolute;">
    @yield('content')
</div>

</body>
</html>