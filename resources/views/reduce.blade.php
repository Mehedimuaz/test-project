@extends('layouts.app')

@section('content')


    <p>
        {{$message}}
    </p>

    <script>
        let users = [
            {
                name: "A",
                age: 34
            },
            {
                name: "V",
                age: 65
            },
            {
                name: "U",
                age: 45
            },
            {
                name: "P",
                age: 25
            },
            {
                name: "B",
                age: 65
            },
            {
                name: "D",
                age: 21
            },
            {
                name: "E",
                age: 39
            },
            {
                name: "Z",
                age: 46
            },
            {
                name: "Y",
                age: 52
            },

        ];

        console.log("Printing the array in JSON format:");
        console.log(JSON.stringify(users));

        console.log("Calculating sum of all users' age by with reduce()");
        let ageSum = users.reduce(function(accumulator, currentValue) {
            return accumulator + currentValue.age;
        }, 0);

        console.log("Printing total age:");
        console.log(ageSum);

        console.log("Calculating sum of square of all users' age by with both map() and reduce()");
        let sqSum = users.map(user => user.age * user.age).reduce((acc, cur) => (acc + cur), 0);

        console.log("Printing sum of square of age:");
        console.log(sqSum);
    </script>

@endsection
