@extends('layouts.app')

@section('content')


    <p>
        {{$message}}
    </p>

    <script>
        let users = [
            {
                name: "A",
                age: 34
            },
            {
                name: "V",
                age: 65
            },
            {
                name: "U",
                age: 45
            },
            {
                name: "P",
                age: 25
            },
            {
                name: "B",
                age: 65
            },
            {
                name: "D",
                age: 21
            },
            {
                name: "E",
                age: 39
            },
            {
                name: "Z",
                age: 46
            },
            {
                name: "Y",
                age: 52
            },

        ];

        console.log("Printing the array in JSON format:");
        console.log(JSON.stringify(users));

        console.log("Sorting the array in ascending order with respect to age")
        users.sort(function(a, b) {
            return a.age - b.age;
        });

        console.log("Printing sorted array using foreach:");
        users.forEach(function(user) {
            console.log("Name of user: " + user.name, ", Age of user: " + user.age);
        });
    </script>

@endsection