@extends('layouts.app')

@section('content')


<p>
    {{$message}}
</p>

    <table border="1">
        <tr>
            <td rowspan="2">6a</td>
            <td>Question:What do you think about the current condition of software development in bangladesh and how this differ from the other developed countries? Also put a comment how we can improve ourselves as a giant digital developed nation in this field?</td>
        </tr>
        <tr>
            <td>Answer: The current situation of software development development needs a lot of improvement. The developers lack necessary skills, the employees do not think from company's perspective, the companies often do not think about the development of employees. The whole world has progressed far ahead of us in technology. Our neighboring country India made great progress in the last decade. The government needs to step forward and make the policies more tech friendly. Even today, many tech companies are in distress due to Covid-19 situation. But the government is not paying enough attention to this sector. The perspective of general people should also be changed in order to fix the problems. We have to work harder and stay up to date with the latest technologies which are being adopted elsewhere.</td>
        </tr>
        <tr>
            <td rowspan="2">6b</td>
            <td>Question: Two fathers & two sons went for fishing. They caught 3 fish. How its possible to equally distribute these 3 fish among all of them without piecing and without killing any of them?</td>
        </tr>
        <tr>
            <td>Answer: There can be two answers if we think from different perspectives.<br>
                1. The fishes can be pieced and distributed among the 4 people. In that way, the two sons and two fathers will not be harmed. It is mentioned in the question that the fathers and sons cannot be pieced or killed.<br> 2. There were actually 3 people. The grandfather, the father, and the son. So, it can be said that there are two fathers and two sons. So 3 fishes can easily be distributed among 3 people.
            </td>
        </tr>
    </table>
@endsection