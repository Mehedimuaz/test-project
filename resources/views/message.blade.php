@extends('layouts.app')

@section('content')


<p>
    {{$message}}
</p>

    <script>
        function checkAge(data, callback)
        {
            if(data.age < 18)
            {
                console.log("Email is not valid");
            }
            else
            {
                callback(data.email);
            }
        }


        let data = {email:'trump@gmail.com', age:70};
        console.log(data);
        checkAge(data, function(email){
            console.log('Email is valid');
        });

        data = {email:'trump@gmail.com', age:14};
        console.log(data);
        checkAge(data, function(email){
            console.log('Email is valid');
        });

    </script>

@endsection