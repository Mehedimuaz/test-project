@extends('layouts.app')

@section('content')


    <p>
        {{$message}}
    </p>

    <script>
        let users = [
            {
                name: "A",
                age: 34
            },
            {
                name: "V",
                age: 65
            },
            {
                name: "U",
                age: 45
            },
            {
                name: "P",
                age: 25
            },
            {
                name: "B",
                age: 65
            },
            {
                name: "D",
                age: 21
            },
            {
                name: "E",
                age: 39
            },
            {
                name: "Z",
                age: 46
            },
            {
                name: "Y",
                age: 52
            },

        ];

        console.log("Printing the array in JSON format:");
        console.log(JSON.stringify(users));

        console.log("Filtering the users within age range of 40-60");
        users = users.filter(function(user) {
            return user.age >=40 && user.age <=60;
        });

        console.log("Printing filtered array in JSON format:");
        console.log(JSON.stringify(users));
    </script>

@endsection