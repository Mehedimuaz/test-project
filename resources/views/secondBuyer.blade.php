@extends('layouts.app')

@section('content')

    <table border="1">
        <tr>
            <th>Buyer id</th>
            <th>Buyer Name</th>
            <th>Total Diary Taken</th>
            <th>Total Pen Taken</th>
            <th>Total Eraser Taken</th>
            <th>Total Items Taken</th>
        </tr>
        @foreach($buyerDetail as $buyer)
            <tr>
                <td>{{$buyer->buyerID}}</td>
                <td>{{$buyer->name}}</td>
                <td>{{$buyer->totalDiary}}</td>
                <td>{{$buyer->totalPen}}</td>
                <td>{{$buyer->totalEraser}}</td>
                <td>{{$buyer->total}}</td>
            </tr>
        @endforeach
    </table>




@endsection