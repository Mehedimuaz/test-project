@extends('layouts.app')

@section('content')
    <style>
        #animation-box {
            width: 10vw;
            height: 10vh;
            background-color: red;
            animation-name: translation;
            animation-duration: 7s;
        }

        @keyframes translation {
            0% {
                transform: translatex(0%) translatey(0%)
            }
            50% {
                transform: translatex(89vw) translatey(79vh)
            }
            100% {
                transform: translatex(0%) translatey(0%)
            }
        }
    </style>

    <div id="animation-box"></div>
<p>
    {{$message}}
</p>


@endsection