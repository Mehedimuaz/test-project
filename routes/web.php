<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('second-buyer-eloquent', 'BuyerController@secondBuyerEloq');
Route::get('second-buyer-no-eloquent', 'BuyerController@secondBuyerNoEloq');
Route::get('purchase-list-eloquent', 'BuyerController@purchaseListEloq');
Route::get('purchase-list-no-eloquent', 'BuyerController@purchaseListNoEloq');
Route::get('record-transfer', 'UtilityController@recordTransfer');
Route::get('define-callback-js', 'UtilityController@defineCallbackJS');
Route::get('sort-js', 'UtilityController@sortJS');
Route::get('filter-js', 'UtilityController@filterJS');
Route::get('map-js', 'UtilityController@mapJS');
Route::get('reduce-js', 'UtilityController@reduceJS');
Route::get('animation', 'UtilityController@animation');
Route::get('i-m-funny', 'UtilityController@funny');